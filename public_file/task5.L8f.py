print("--->BODY MASS INDEX<---")


def body_mass(weight, height):

    return weight / height ** 2


while True:
    print("Enter to continue: ")
    options = input("Start or off ---> ")
    if options == "Start":
        print("Enter data for calculation: ")
        height = float(input("Enter your height in m: "))
        weight = float(input("Enter your weight in kg: "))
        imt_result = round(body_mass(weight, height), 2)
        print(imt_result)
        if 18 <= imt_result <= 25:
            print("Body weight is normal")
        elif 16 <= imt_result < 18:
            print("Insufficient weight")
        elif 25 < imt_result <= 30:
            print("Overweight")
        continue
    if options == "off":
        print("Exit the program!")
        break
