def number_1(x):
    result = (3 * x + 24)
    return result


def number_2(x):
    result1 = (x * (5 - 6))
    return result1


print(" x   | n1(x) | n2(x)")
print("-----|-------|-------")

for x in range(-10, 11, 1):
    x /= 2
    print(f"{x:>4.1f} | {number_1(x):>5.1f} | {number_2(x):>5.1f}")
