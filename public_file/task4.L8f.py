print("Calculating the arithmetic mean of given numbers")
print("Options for arithmetics mean:")
print("1 - Ender your numbers")
print("2 - continue or quit?")


def number(x, y, z):
    return (x + y + z) / 3


while True:
    options = int(input("Options 1 or 2?\n"))

    if options == 1:
        x = int(input("Enter First number: "))
        y = int(input("Enter Second number: "))
        z = int(input("Enter Third number "))
        result = number(x, y, z)

        print(f"Result arithmetic mean: {int(result)}")

    elif options == 2:
        print("Thanks for using this program:)")
        break
    else:
        print("Exit")



