import math
print("---->PROGRAM-CALCULATOR<----"'\n')


print("""Options of calculators menu:
1 - for addition '+'
2 - for subtraction '-'
3 - for multiplication '*' 
4 - for division '/' 
5 - for exponentiation '**' 
6 - for reduction to square 'sqrt2'
7 - for cube roots 'sqrt3' 
8 - close calculator and exit """)


def calculate():

    while True:
        operation = int(input("Enter the selected option: " '\n'))

        if operation == 1:
            num1 = int(input("Enter first number: "))
            num2 = int(input("Enter second number: "))
            addition = num1 + num2
            print("Result addition :", addition)
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 2:
            num1 = int(input("Enter first number: "))
            num2 = int(input("Enter second number: "))
            subtraction = num1 - num2
            print("Result subtraction :", subtraction)
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 3:
            num1 = input("Enter first number: ")
            num2 = input("Enter second number: ")
            if "." in num1 or "." in num2:
                num1 = float(num1)
                num2 = float(num2)
            else:
                num1 = int(num1)
                num2 = int(num2)

            multiplication = num1 * num2
            print("Result multiplication :", multiplication)
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 4:

            while True:
                num1 = input("Enter first number: ")
                num2 = input("Enter second number: ")
                if "." in num1 or "." in num2:
                    try:
                        num1 = float(num1)
                        num2 = float(num2)
                        break
                    except ValueError:
                        print("Error: input cannot contain 'letters', must be a number!")
                        continue
                else:
                    try:
                        num1 = int(num1)
                        num2 = int(num2)
                        break
                    except ValueError:
                        print("Error: input cannot contain 'letters', must be a number!")
                        continue

            if num1 != 0 and num2 != 0:
                division = num1 / num2
                print("Result division:", division)
            else:
                print("Error: division by zero is not possible!")
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 5:
            num = float(input("Enter number: "))
            power = float(input("enter number to exponentiate: "))
            exponentiation = num ** power
            print("Result exponentiation:", exponentiation)
            if exponentiation.is_integer():
                print("Result exponentiation, integer output:", int(exponentiation))
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 6:
            num = float(input("Enter first number: "))
            square = math.sqrt(num)
            print("Result square:", square)
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 7:
            num = float(input("Enter first number: "))
            cube = math.pow(num, 1/3)
            print("Result cube:", cube)
            if input("Press Enter to continue or 'q' to quit: ").lower() == 'q':
                break

        elif operation == 8:
            print("Stop working with the calculator and close the program")


calculate()



